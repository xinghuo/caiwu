
module.exports = function ( mongoose){
     var collection = 'tudos'
	     ,Schema    = mongoose.Schema
		 ,ObjectId  = Schema.ObjectId;

     var schema = new Schema({
			user_id    : String,
			content    : String,
			updated_at : Date
		});
 
     this.model = mongoose.model(collection, schema);
    
	 return this;

}