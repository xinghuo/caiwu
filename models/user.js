module.exports = function (){
	console.log('loading model user ');
    var mongoose = require('../models/base')().mongoose;
	
    var collection = 'users'
    ,Schema    = mongoose.Schema
    ,ObjectId  = Schema.ObjectId;

		 var schema = new Schema({
				email      : String,
				nick       : String,
				password   : String,
				updated_at : Date
			});
	 
		 this.model = mongoose.model(collection, schema);
 
	this.find = function ( where,callback){
		this.model.findOne( where, function ( err, user ){
			callback(err,user);
	    });
         
	}
    return this;
}
