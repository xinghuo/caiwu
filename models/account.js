/**
* 账目表
*/
module.exports = function ( mongoose){
    var mongoose = require('../models/base')().mongoose;
	 
    var collection = 'accounts'
    ,Schema    = mongoose.Schema
    ,ObjectId  = Schema.ObjectId;
    var schema = new Schema({
        user_id    : String,
        custom_type : Number,
        content    : String,
        come_type  : Number,
        price      : Number,
        custom_at : Date,
        created_at : Date,
        updated_at : Date
    });
 
    this.model = mongoose.model(collection, schema);
    
    return this;

}