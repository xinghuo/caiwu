
/*
 * GET home page.
 */
var  utils    = require('../common/utils');
var  mongoose = require('mongoose')
var  mUser     = require('../models/user')();
var  mAccount    = require('../models/account')();
exports.index = function(req, res){
       console.log("the request url is " + req.url );
	   res.render('static', {  title: 'title'
	   });
};
exports.test = function(req, res){
	console.log(req.body.bbk );
				mUser.model({ 
						user_id : req.query["bbk"]
			  }).save( function( err, todo, count ){
					 console.log(err);	 console.log(count);
				req.session.logged = true;
				res.send(todo);
				//res.redirect( '/user/' );
			  });

		
};
exports.list = function(req, res){
    Todo.find().sort( {'updated_at' : -1}).execFind( function ( err, todos, count){
       res.render( 'index',{
	         title: 'Todo Example' + count,
	         todos: todos
	   });
     
   });
};

 
exports.feed = function(req, res){
  res.render('login.ejs', { title: 'feed' });
};
  
exports.create = function ( req, res ){
  console.log('create ing');
  new Todo({
    content    : req.body.content,
    updated_at : Date.now()
  }).save( function( err, todo, count ){
    res.redirect( '/' );
  });
};

exports.destroy = function ( req, res ){
   Todo.findById( req.params.id, function ( err, todo ){
       todo.remove( function ( err, todo){
	      res.redirect( '/' );
	   });
   });
} 

exports.edit = function ( req, res ){
 
   Todo.find( function ( err, todos){
         res.render( 'edit', {
		    title   : 'Express Edit',
            todos   : todos,
		    current : req.params.id
		 });
   });
}

exports.update = function( req, res ){
    Todo.findById( req.params.id, function ( err, todo ){
	   todo.content = req.body.content;
	   todo.updated_at = Date.now();

	   todo.save( function ( err, todo, count){
	          res.redirect( '/?'+count );
	   });
	});
  
};