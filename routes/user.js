
/*
 * GET users listing.
 */

/**
* 登录页面
*/
var  utils    = require('../common/utils');
var  mongoose = require('mongoose')
var  mUser     = require('../models/user')();
var  mAccount    = require('../models/account')();
exports.index = function(req, res){
    var mAcount = mAccount.model.count({user_id:req.session.user_id});
    var perPage = 20;
    var pages = Math.ceil(mAccount / perPage);
    var page  = Math.abs(req.body.page);
    var before  = page > 1 ?  page -1 : 1;
    var next =  page > pages ? pages : page + 1;
    var skipCount = (page - 1)*perPage;
    mAccount.model.find().skip(skipCount).limit(perPage).sort( {'created_at' : -1}).execFind( function ( err, accounts, count){
        for ( var k in accounts ){
                   
                        accounts[k].custom_ymdhis = utils.format_date(new Date(accounts[k].custom_at));
                        
                         
        }
     
       res.render( 'user/index',{
	         title: '账单明细' ,
	         accounts: accounts
       });
     
   });
  
};
exports.login = function(req, res){
  res.render('login');
};

exports.register = function(req, res){
  res.render('login');
};

exports.doLogin = function(req, res){
  res.render('login');
};