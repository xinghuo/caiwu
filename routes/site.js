
/*
 * GET home page.
 */
var  mAccount    = require('../models/account')();
var  utils    = require('../common/utils');
  
exports.index = function(req, res){
    if (req.session.logged){
	   res.redirect('/user/');
    }else {
			res.redirect('/site/login');
    }
};
 

exports.login = function(req, res){
   console.log("site login loading");
   res.render('login',  {title:global.pageTitle} );
};
exports.statusLogin = function(req, res){
  if(req.session.logged){
	  if(!req.session.nick){
	    req.session.nick = '亲,';
	  }
     var status = '<li><a href="#">'+req.session.nick+' 欢迎您!</a></li>';
  }else{
     var status = '<li><a href="/site/login">朋友 欢迎您! 马上登陆</a></li>';
  }
  status += '<li><a href="/site/logout">退出</a></li>';
    res.send('document.write(\''+status+'\')'); 
}
exports.doLogin = function(req, res){
	var  mUser    = require('../models/user')();
    if (req.session.logged){
		res.redirect('/user/');
    }else {
		 var pwd_in = utils.md5(req.body.pwd_in);
                 console.log('pageTitle:' + global.pageTitle );
              
		 mUser.find({email:req.body.email_in,password:pwd_in},function ( err, user){
			     
			     if( user != null){
                                        console.log(' user.id is ' +  user.id);
                                        req.session.user_id = user.id;
					req.session.logged = true;
					req.session.nick = user.nick;
					console.log('req.session.logged ' + req.session.logged);
				    utils.message(res,{message:'登录成功!',redirect:'/user',title:global.pageTitle},'alert');
				 }else{
				    utils.message(res,{message:'亲.你登录的账号不对哦!',redirect:'/site/login',title:global.pageTitle},'alert');
				 }
        
		 });
	
      
    }
};

exports.doRegister = function ( req, res ){
       var  mUser    = require('../models/user')();

	   mUser.find({email:req.body.email_up},function ( err, user){
	      console.log(user + req.body['email_up']);
		  if(user){
			  utils.message(res,{message:'亲.用户已存在!',title:global.pageTitle,redirect:'/site/login'},'alert');
		  }else{
			  mUser.model({
				email    : req.body['email_up'],
				nick     : req.body['nick_up'],
				password : utils.md5(req.body.pwd_up),
				updated_at : Date.now()
			  }).save( function( err, todo, count ){
			        req.session.logged = true;
					req.session.nick = todo.nick;
				    res.redirect( '/user/' );
			  });
		  }
	 });
 
};

exports.logout = function ( req, res ){
	  req.session.logged = false;
      utils.message(res,{message:'亲.您已经退出登录!',title:global.pageTitle,redirect:'/site/login'},'alert');
} 
