module.exports = function( app ){
	
    //配置
    var cSite = require('./config/site')();
        pageTitle = cSite.page.title;
        pageDesc  =  cSite.page.desc;
        projectAuth = cSite.projectAuth;
	//登录注册 路由设置
	var rSite   = require('./routes/site');
	app.get('/site/login', rSite.login);
    app.get('/site/status/login',rSite.statusLogin);
    app.get('/site/logout',rSite.logout);
	app.post('/site/doLogin', rSite.doLogin);
	app.post('/site/doRegister', rSite.doRegister);
	app.get('/site/doRegister', function( req, res){
            res.redirect('/site/login');
    });
    app.post('/site/*', function( req, res){
            res.redirect('/site/login');
    });

	var rUser   = require('./routes/user');
    //登录检测
	app.get( '/user*', function( req, res, next){
		 if (!req.session.logged){
			res.redirect('/site/login');
		 }else{
		    	next();
		 }
	});
	app.get('/user', rUser.index);
        
        var rAccount   = require('./routes/account');
        app.post('/account/doAddAccount',rAccount.doAddAccount);
        app.post('/account/doDelete',rAccount.doDelete);
        app.get('/account/accountStatus',rAccount.accountStatus);
        
	var routes = require('./routes/index')
	app.get('/', routes.index);
		app.get('/test', routes.test);
	app.post( '/create', routes.create ); 
	app.get( '/destroy/:id', routes.destroy ); 
	app.get( '/edit/:id', routes.edit );  
	app.post( '/update/:id', routes.update );
	app.get ( '/list', routes.list );

	   

	app.get('/feed', routes.feed);
	
//	app.get('*',function (req, res){
//		    	 res.send('404');
//	});
};